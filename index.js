console.log('Hello World')
console.log(document)
console.log(document.querySelector('#txt-first-name'))

/*
  document
    - refers to the whole web page
  querySelector
    - used to select a specific element (obj) as long as it is inside the <html> (HTML ELEMENT)
    - takes a string input that is formatted just like CSS Selector
    - can select elements regardless if the string is an id, class, or a tag as long as the element is existing in the webpages
*/

/*
  Alternative methods that we use aside from querySelector in retrieving elements:
    1) document.getElementById()
    2) document.getElementByClassName()
    3) document.getElementByTagName()
*/

const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name')

txtFirstName.addEventListener(
  'keyup',
  (event) => (spanFullName.innerHTML = txtFirstName.value)
)

txtLastName.addEventListener(
  'keyup',
  (event) =>
    (spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`)
)
